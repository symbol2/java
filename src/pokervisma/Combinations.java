/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokervisma;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author Donatas
 */
 class Combinations {
        public int firstSuit;
        public int setScore = 0;
        public HashMap<Integer,Card> cardsToKeepMap;
        public static Deck deck = new Deck();
        private List<Card> playerCards;
        public ArrayList<Integer> sortCardRanks = new ArrayList<>();
    	public ArrayList suits = new ArrayList<>();       
        private Scanner scanner;      
        public List<Card> dealCards(){
            deck.shuffle();
            deck.remainingCards();
            //System.out.println("\nDeck: \n"+deck.cardDeck);
    	try{
    		playerCards = deck.dealCards(5);
    	}
    	catch(GameExc e){
    		System.out.println("GameExc: " + e.getMessage());
    	}
                System.out.println("Your cards:             "+playerCards+"\n");
        return playerCards;
    }
        
        public void cardsToKeep(){
    	System.out.print("\nWhich cards you want to K E E P (1-5)(separated with spaces)\nIf you want to continue without changing cards pres ENTER\n---|");

    	//Create new instance of scanner and get input line
    	scanner = new Scanner(System.in);
    	String input = scanner.nextLine();

    	if(input.isEmpty()){
    		return;
    	}
        String[] positionsToKeep = input.trim().split("\\s+");

    	try{
    		for(int i = 0; i < positionsToKeep.length; i++){
    			int position = Integer.parseInt(positionsToKeep[i]) - 1;
    			Card card = playerCards.get(position);
    			cardsToKeepMap.put(position, card);
    		}
    	}
        catch(Exception e){
    		System.out.println("\nPlease input integers 1-5 only. Try again\n");
    		cardsToKeep();
    	}
    }      
            public void showNewDraw(){
    	checkCards();
    	for(Map.Entry<Integer, Card> card : cardsToKeepMap.entrySet()){
    		playerCards.set(card.getKey(), card.getValue());
    	}
		
		System.out.println(playerCards.toString());
    }   
    Combinations(){

		scanner = new Scanner(System.in);
		cardsToKeepMap = new HashMap<>();
    }   
    
//Combinations        
        public boolean royalFlush(){          
    	firstSuit = playerCards.get(0).getSuit();
    	List<Integer> royalFlushRankList = Arrays.asList(8,9,10,11,12);
    	for(Card card : playerCards){
    		if(card.getSuit() != firstSuit || !royalFlushRankList.contains(card.getRank()))
    			return false;
    	}
    	return true;
        }      
        public boolean straightFlush(){
    	firstSuit = playerCards.get(0).getSuit();
    	List<Integer> sortCardRanks = new ArrayList<>();
    	for(Card card : playerCards){
    		sortCardRanks.add(card.getRank());
    	}
    	Collections.sort(sortCardRanks);
    	for(Card card : playerCards){
    		if(card.getSuit() != firstSuit)
    			return false;
    	}
    	for(int i = 0; i < 4; i++){
    		if(!(sortCardRanks.get(i) == (sortCardRanks.get(i+1) - 1)))
    			return false;
        }
    	return true;
    }
        public boolean ofAKind(int numOfSameRanks){
    	HashMap<Integer,Integer> ranks = new HashMap<>();
    	for(Card card : playerCards){
    		if(!ranks.containsKey(card.getRank())){
    			ranks.put(card.getRank(), 1);
    		}
    		else{
    			int value = ranks.get(card.getRank());
    			ranks.put(card.getRank(), value+1);
                        //System.out.println("How many same cards found: "+(value+1));
    		}
    	}           
    	return ranks.containsValue(numOfSameRanks);
    }       
        public boolean fullHouse(){
    	HashMap<Integer,Integer> ranks = new HashMap<>();
    	for(Card card : playerCards){
    		if(!ranks.containsKey(card.getRank())){
    			ranks.put(card.getRank(), 1);
    		}
    		else{
    			int value = ranks.get(card.getRank());
    			ranks.put(card.getRank(), value+1);
    		}
    	}
    	return ranks.containsValue(3) && ranks.containsValue(2);
    }
        public boolean flush(){
    	int firstSuit = playerCards.get(0).getSuit();
    	for(Card card : playerCards){
    		if(card.getSuit() != firstSuit)
    			return false;
    	}
    	return true;
    }
	public boolean straight(){
    	int firstSuit = playerCards.get(0).getSuit();
    	for(Card card : playerCards){
    		sortCardRanks.add(card.getRank());
    		suits.add(card.getSuit());
    	}
    	Collections.sort(sortCardRanks);
    	Set<Integer> suitSet = new HashSet<>(suits);
    	if(suitSet.size() > suits.size())
    		return false;
    	for(int i = 0; i < 4; i++){
    		if(!(sortCardRanks.get(i) == (sortCardRanks.get(i+1) - 1)))
    			return false;
    	}
    	return true;
    }
    public boolean twoPairs(){
        int pairs = 0;
    	HashMap<Integer,Integer> ranks = new HashMap<>();
    	for(Card card : playerCards){
    		if(!ranks.containsKey(card.getRank())){
    			ranks.put(card.getRank(), 1);
    		}
    		else{
    			int value = ranks.get(card.getRank());
    			ranks.put(card.getRank(), value+1);
    			pairs++;
    		}
    	}
    	return pairs == 2 && ranks.containsValue(1);
    }
            public boolean jacksOrBetter(){
    	HashMap<Integer,Integer> ranks = new HashMap<>();
    	for(Card card : playerCards){
            if(!ranks.containsKey(card.getRank())){
                ranks.put(card.getRank(), 1);
            }else if(card.cardRank>=9){
    			int value = ranks.get(card.getRank());
    			ranks.put(card.getRank(), value+1);
    		}    
            }
    	return ranks.containsValue(2);
    }       
        public void checkCards(){
        dealCards();    
    	if(royalFlush() == true){
                setScore = setScore+800;
    		System.out.println("You've got a Royal Flush!\nCurrent score: "+setScore);   		
    	}else if(straightFlush()){
                setScore = setScore+50;
    		System.out.println("You've got a Straight Flush!\nCurrent score: "+setScore);	
    	}else if(ofAKind(4)){
                setScore = setScore+25;
    		System.out.println("You've got Four of a kind!\nCurrent score: "+setScore);
    	}else if(fullHouse()){
                setScore = setScore+9;
    		System.out.println("You've got a Full House!\nCurrent score: "+setScore);
    	}else if(flush()){
                setScore = setScore+6;
    		System.out.println("You've got a Flush!\nCurrent score: "+setScore);
    	}else if(straight()){
                setScore = setScore+4;
    		System.out.println("You've got a Sraight!\nCurrent score: "+setScore);
    	}else if(ofAKind(3)){
                setScore = setScore+3;
    		System.out.println("You've got Three of a kind!\nCurrent score: "+setScore);
        }else if(twoPairs()){
                setScore = setScore+2;
    		System.out.println("You've got Two pairs!\nCurrent score: "+setScore);               
    	}else if(jacksOrBetter()){
                setScore = setScore+1;
    		System.out.println("You've got a pair of Jacks or better!\nCurrent score: "+setScore);   		               
    	}           
        else{
            System.out.println("Current score: "+setScore);
        }
}      
}
