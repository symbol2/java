/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokervisma;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Donatas
 */
class GameExc extends Exception {
    GameExc (){
		super ();
    }
    GameExc ( String reason ){
		super ( reason );
    }
}
class Card {   
    static final String[] Rank = {"2","3","4","5","6","7","8","9","10","J","Q","K","A"};  
    static final String[] Suit = {"Clubs", "Diamonds", "Hearts", "Spades" };    
    public int cardRank;
    public int cardSuit;    
    public Card(int rank, int suit) throws GameExc {
        cardRank = rank;
        cardSuit = suit;
    }
    public int getRank(){
        return cardRank;
    }
    public int getSuit(){
        return cardSuit;
    }
    @Override
    public String toString(){
        return Rank[cardRank]+" of "+Suit[cardSuit];}
}
    class Deck{
        public List<Card> cardDeck;
        public ArrayList<Card> playerCards;       
        public Deck(){
        cardDeck = new ArrayList<>();
        try{
            for(int rank = 0; rank < 13; rank++){
                for(int suit = 0; suit < 4; suit++){
                	Card newCard = new Card(rank, suit);
                        cardDeck.add(newCard);
                    //System.out.println("Card: "+newCard);
                }
            }                   
        }
        catch(GameExc e){
        	System.out.println("\nException found: \n" + e.getMessage());
        }
    }
    public void shuffle(){
        Collections.shuffle(cardDeck);        
    }   
    public ArrayList<Card> dealCards(int cards) throws GameExc{
        ArrayList<Card> usedCards = new ArrayList<>();
                	if(5 > remainingCards()){
        		throw new GameExc("\nNot enough cards in deck. Select less.\n");
        	}
		    for(int i = cards-1; i >= 0; i--){
		    	Card firstFive = cardDeck.remove(i);
		    	usedCards.add(firstFive);                       
        	}
                    System.out.println("\nCards left in deck:     "+cardDeck.size());
        return usedCards;
    }   
        public int remainingCards()
    {   
	return cardDeck.size();
    }
    public String toString()
    {
	return ""+cardDeck;
    } 
    }
